khmer(1)
========
:doctype: manpage

NAME
----
khmer - k-mer counting, filtering and graph traversal FTW!

SYNOPSIS
--------

 khmer `<prog_name>` [options] <args>

eg.

 khmer `load-into-counting` -x 1e8 -k 20 reads.ct reads.fa.gz

## USAGE

This manpage only describes the `khmer` wrapper script.  To find out about
the software and how to use it please see /usr/share/doc/khmer or else
http://khmer.readthedocs.org/.

On Debian/Ubuntu, package maintainers avoid putting large collections of scripts
directly into the system PATH.  Rather, a single wrapper script is used to launch
the script you want to use; therefore if the khmer instructions say to run
`load-into-counting.py ...whatever...` you need to prefix this command with
`khmer`.  Furthermore, the .py extension may be omitted, resulting in a
command of the format shown above.

If you don't like this, simply add /usr/lib/khmer/bin to your personal PATH.

## AUTHOR

This wrapper and manpage were written by Tim Booth, who takes no credit for
khmer itself.
